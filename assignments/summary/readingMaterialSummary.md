# IoT Basics

## Industrial Revolution

- Industry 1.0 (1784) : Mechanization, Steam Power and Weaving loom.
- Industry 2.0 (1870) : Mass Production, Assembly line and Electrical energy.
- Industry 3.0 (1969) : Automation, Computers and Electronics.
- Industry 4.0 (Today) : Cyber Physical Systems, Internet of things and Network.

![Industry Revolution](extras/Industry_Revolution.jpg)

## Industry 3.0

Data is stored in databases and represented in excels. 

#### Architecture:

![3.0 Architecture](extras/3.0_Architecture.PNG)

#### Communication Protocols:

- Modbus
- Profinet
- CANopen
- EtherCAT

These protocols are used by sensors to send data to PLC's, called Fieldbus.

## Industry 4.0

Industry 3.0 + Internet (IoT) = Industry 4.0

<b>Internet</b> sends you data.

#### Architecture:

![4.0 Architecture](extras/4.0_Architecture.PNG)

#### Communication Protocols:

- MQTT.org
- AMQP
- OPC UA
- CoAP RFC 7252
- Websockets
- HTTP
- Restful API

These protocols are optimized for sending data to cloud for data analysis.

## Problems with Industry 4.0 upgrades:

- Cost
- Downtime
- Reliability

#### Solution (follow the below steps):

1. Identify most popular Industry 3.0 devices.
2. Study Protocols that these devices communicate.
3. Get data from the Industry 3.0 devices.
4. Send the data to cloud for Industry 4.0.

#### Tools used to analyse data:

- <b>IoT TSDB tools:</b> Store your data in Time series databases (eg. Prometheus, InfluxDB)
- <b>IoT Dashboards:</b> View all your data into beautiful dashboards (eg. Grafana, Thingsboard)
- <b>IoT Platforms:</b> Analyse your data on these platforms (eg. AWS IoT, Google IoT, Azure IoT, Thingsboard)

For getting alerts use platforms like <b>Zaiper, Twilio.</b>

## Applications of Industry 4.0:

![Industry4.0](extras/Industry4.0.jpg)



